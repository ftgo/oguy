#!/bin/bash

set -e

. $(dirname "$0")/.env

echo "mBot - back 450"

# curl --location --request POST "${IOTA}/v2/op/update" \
# --header 'Content-Type: application/json' \
# --header 'fiware-service: oguy' \
# --header 'fiware-servicepath: /' \
# --data-raw '{
#     "actionType": "update",
#     "entities": [
#         {
#             "type": "Thing",
#             "id": "urn:ngsi-ld:Mbot:001",
#             "back" : {
#                 "type": "command",
#                 "value": "450"
#             }
#         }
#     ]
# }'

curl --location --request PUT "${ORION}/v2/entities/urn:ngsi-ld:Mbot:001/attrs/back/value?type=Thing" \
--header 'Content-Type: text/plain' \
--header 'fiware-service: oguy' \
--header 'fiware-servicepath: /' \
--data-raw '450'

# curl  --location --include --request PATCH "${ORION}/v2/entities/urn:ngsi-ld:Mbot:001/attrs?type=Thing" \
# --header 'Content-Type: application/json' \
# --header 'fiware-service: oguy' \
# --header 'fiware-servicepath: /' \
# --data-raw '{
#   "back": {
#       "type" : "command",
#       "value" : 450
#   }
# }'

# curl --location --include --request POST "${ORION}/v2/op/update" \
# --header 'Content-Type: application/json' \
# --header 'fiware-service: oguy' \
# --header 'fiware-servicepath: /' \
# --data '{
#   "actionType":"update",
#   "entities":[
#     {
#       "id":"urn:ngsi-ld:Mbot:001", "type":"Thing",
#       "back": {
#         "type" : "command",
#         "value" : "450"
# 	  }
#     }
#   ]
# }'
