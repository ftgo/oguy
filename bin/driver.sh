#!/bin/bash

# docker-compose --file docker/orion/docker-compose.yml up --detach
# docker-compose --file docker/cygnus/docker-compose.yml up --detach

BASEDIR=$(dirname "$0")

cd ${BASEDIR}/../src/oguy/oguy-driver/

# JAVA_HOME=/opt/usr/bin/java/openjdk-15.0.1
JAVA_HOME=/opt/usr/bin/java/openjdk-14

mvn install

# kill -9 $(ps -aux | grep oguy-driver | awk '{print $2}') 2>/dev/null
${JAVA_HOME}/bin/java -jar target/oguy-driver.jar
