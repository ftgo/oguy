#!/bin/bash

set -e

. $(dirname "$0")/.env

echo "mBot - line 1000"

# curl --location --request POST "${IOTA}/v2/op/update" \
# --header 'Content-Type: application/json' \
# --header 'fiware-service: oguy' \
# --header 'fiware-servicepath: /' \
# --data-raw '{
#     "actionType": "update",
#     "entities": [
#         {
#             "type": "Thing",
#             "id": "urn:ngsi-ld:Mbot:001",
#             "line" : {
#                 "type": "command",
#                 "value": "1000"
#             }
#         }
#     ]
# }'

curl --location --request PUT "${ORION}/v2/entities/urn:ngsi-ld:Mbot:001/attrs/line/value?type=Thing" \
--header 'Content-Type: text/plain' \
--header 'fiware-service: oguy' \
--header 'fiware-servicepath: /' \
--data-raw '1000'

# curl  --location --include --request PATCH "${ORION}/v2/entities/urn:ngsi-ld:Mbot:001/attrs?type=Thing" \
# --header 'Content-Type: application/json' \
# --header 'fiware-service: oguy' \
# --header 'fiware-servicepath: /' \
# --data-raw '{
#   "line": {
#       "type" : "command",
#       "value" : "1000"
#   }
# }'

# curl --location --include --request POST "${ORION}/v2/op/update" \
# --header 'Content-Type: application/json' \
# --header 'fiware-service: oguy' \
# --header 'fiware-servicepath: /' \
# --data '{
#   "actionType":"update",
#   "entities":[
#     {
#       "id":"urn:ngsi-ld:Mbot:001", "type":"Thing",
#       "line": {
#         "type" : "command",
#         "value" : "1000"
# 	  }
#     }
#   ]
# }'
