#!/bin/bash

# docker-compose --file docker/orion/docker-compose.yml up --detach
# docker-compose --file docker/cygnus/docker-compose.yml up --detach

BASEDIR=$(dirname "$0")

cd ${BASEDIR}/../docker/oguy

./services start

cd - > /dev/null
