#!/bin/bash

# docker-compose --file docker/cygnus/docker-compose.yml stop
# docker-compose --file docker/orion/docker-compose.yml stop

BASEDIR=$(dirname "$0")

cd ${BASEDIR}/../docker/oguy

./services stop

cd - > /dev/null
