#!/bin/bash

set -e

echo "tunnel - localhost:1026 @ https://oguy.loca.lt"

lt --subdomain oguy --print-requests --allow-invalid-cert --port 1026
