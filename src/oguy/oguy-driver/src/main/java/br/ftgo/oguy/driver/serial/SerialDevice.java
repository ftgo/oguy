package br.ftgo.oguy.driver.serial;

import br.ftgo.oguy.driver.model.Ultralight;
import com.fazecast.jSerialComm.SerialPort;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.HashMap;

@Component
public class SerialDevice {
    private static final Logger LOGGER = LogManager.getLogger(SerialDevice.class);

    @Autowired
    private SerialReceiver receiver;

    @Autowired
    private SerialProperties properties;

    private final HashMap<String, SerialPort> ports;

    public SerialDevice() {
        this.ports = new HashMap<>();
    }


    @PreDestroy
    public void onExit() {
        for (SerialPort port : this.ports.values()) {
            if (!port.isOpen()) {
                LOGGER.warn("Port closed ({})", port.getSystemPortName());
                return;
            }

            port.removeDataListener();
            port.closePort();
        }
    }

    private SerialPort getSerialPort(String name) {
        SerialPort port = this.ports.get(name);

        if (port == null) {
            var deviceProperties = this.properties.getDevice(name);
            if (deviceProperties == null) return null;

            port = SerialPort.getCommPort(deviceProperties.getPort());
            port.setBaudRate(deviceProperties.getRate());

            port.addDataListener(this.receiver);

            this.ports.put(name, port);
        }


        return port;
    }


    private void write(SerialPort port, byte... bytes) {
        try {
            try (var out = new BufferedOutputStream(port.getOutputStream())) {
                LOGGER.trace("Sending {} + 1 bytes", bytes.length);

                out.write(bytes);
                out.write(0);
                out.flush();
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    private boolean validate(Ultralight ultralight) {
        if (ultralight == null) {
            LOGGER.warn("Ultralight null");
            return false;
        }

        if (ultralight.getName() == null) {
            LOGGER.warn("Ultralight name null");
            return false;
        }

        if (ultralight.getPayload() == null) {
            LOGGER.warn("Ultralight payload null");
            return false;
        }

        return true;
    }

    public void send(Ultralight ultralight) {
        if (!validate(ultralight)) return;

        SerialPort port = getSerialPort(ultralight.getName());
        if (port == null) {
            LOGGER.warn("Device not configured ({})", ultralight.getName());
            return;
        }

        if (!port.isOpen() && !port.openPort()) {
            LOGGER.warn("Unable to open port ({})", port.getSystemPortName());
            return;
        }

        String message = ultralight.toDevice();

        LOGGER.trace("Send: '{}'", message);

        write(port, message.getBytes());
    }
}
