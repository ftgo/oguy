package br.ftgo.oguy.driver.infra;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Aspect
@Component
@ConditionalOnExpression("${aspect.enabled:true}")
public class ClockAdvice {
    private static final Logger LOGGER = LogManager.getLogger(ClockAdvice.class);


    @Around("@annotation(br.ftgo.oguy.driver.infra.Clock)")
    public Object executionTime(ProceedingJoinPoint point) throws Throwable {
        Signature signature = point.getSignature();

        StopWatch clock = new StopWatch();

        clock.start();
        Object object = point.proceed();
        clock.stop();

        LOGGER.info("{} {} {}", signature.getDeclaringTypeName(), signature.getName(), clock.getLastTaskTimeMillis());

        return object;
    }
}