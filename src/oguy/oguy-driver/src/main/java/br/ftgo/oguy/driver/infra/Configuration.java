package br.ftgo.oguy.driver.infra;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean(name = "applicationEventMulticaster")
    public ApplicationEventMulticaster simpleApplicationEventMulticaster() {
        var multicaster = new SimpleApplicationEventMulticaster();
        multicaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
        
        return multicaster;
    }

    @Bean
    public ServletRegistrationBean logServletBean() {
        var bean = new ServletRegistrationBean(new LogServlet(), "/log/*");
        bean.setLoadOnStartup(1);

        return bean;
    }
}
