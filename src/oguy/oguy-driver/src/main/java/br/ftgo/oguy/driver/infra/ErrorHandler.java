package br.ftgo.oguy.driver.infra;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatus.Series;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.joining;
import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;


public class ErrorHandler implements ResponseErrorHandler {
    private static final Logger LOGGER = LogManager.getLogger(ErrorHandler.class);

    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        Series series = httpResponse.getStatusCode().series();

        return (series == CLIENT_ERROR || series == SERVER_ERROR);
    }

    @Override
    public void handleError(ClientHttpResponse response) {
        try {
            HttpStatus code = response.getStatusCode();

            Series series = code.series();

            if (series == SERVER_ERROR) {
                LOGGER.error("Server error ({})", code);
                return;
            }

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getBody(), UTF_8))) {
                String message = reader.lines().collect(joining("\n"));

/*
                ObjectMapper mapper = new ObjectMapper();
                Mensagem msg = mapper.readValue(json, Mensagem.class);
*/
                LOGGER.error("Client error ({}). Message: '{}'", code, message);
            }

        } catch (IOException e) {
            LOGGER.error(e);
        }
    }
}
