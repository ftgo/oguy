package br.ftgo.oguy.driver.infra;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class LogServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(LogServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("URL: {}", request.getRequestURL());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("URL: {}", request.getRequestURL());
        LOGGER.info("Body: {}", getBody(request));
    }

    private String getBody(HttpServletRequest request) throws IOException {
        StringBuilder buffer = new StringBuilder();

        try (InputStream input = request.getInputStream(); BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
            char[] chars = new char[1024];

            int length = -1;
            while ((length = reader.read(chars)) > 0) {
                buffer.append(chars, 0, length);
            }
        }

        return buffer.toString();
    }
}