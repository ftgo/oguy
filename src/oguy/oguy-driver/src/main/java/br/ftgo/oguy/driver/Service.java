package br.ftgo.oguy.driver;

import br.ftgo.oguy.driver.agent.AgentClient;
import br.ftgo.oguy.driver.infra.Clock;
import br.ftgo.oguy.driver.model.Event;
import br.ftgo.oguy.driver.model.Ultralight;
import br.ftgo.oguy.driver.serial.SerialDevice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

@org.springframework.stereotype.Service
public class Service {
    private static final Logger LOGGER = LogManager.getLogger(Service.class);

    private final HashMap<Long, Ultralight> pool;

    private AtomicBoolean processed = new AtomicBoolean(true);


    @Autowired
    private SerialDevice device;

    @Autowired
    private AgentClient client;

    public Service() {
        this.pool = new HashMap<>();
    }

    @Clock
    @EventListener(condition = "#event.success")
    public void receive(Event<String> event) {
        String message = event.getMessage();

        var ultralight = Ultralight.fromDevice(message);

        // FIXME record on mongodb
        Ultralight original = this.pool.get(ultralight.getId());
        if (original == null) {
            LOGGER.warn("Message not found ({}, {})", message, ultralight);
            return;
        }

        ultralight.setName(original.getName());

        LOGGER.debug("Received: '{}'", ultralight);

        this.client.send(ultralight);
    }

    @Clock
    public String send(String message) {
        Ultralight ultralight = Ultralight.fromAgent(message);

        this.pool.put(ultralight.getId(), ultralight);

        LOGGER.debug("Send: '{}'", ultralight);

        this.device.send(ultralight);

        String payload = ultralight.getPayload();
        String cmd = payload.substring(payload.indexOf('|'));

        // FIXME wait response instead of automatically signaling OK?
        return String.format("%s| %s OK", message, cmd);
    }
}
