package br.ftgo.oguy.driver.agent;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;


@Component
@ConfigurationProperties(prefix = "agent")
public class AgentProperties {
    private String host;
    private Integer port;
    private String path;
    private String key;

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return this.port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        try {
            URL url = new URL("http", this.host, this.port, this.path);
            return url.toString();
        } catch (MalformedURLException e) {
            throw new IllegalStateException("Unable to build URL", e);
        }
    }

    public String getUri(String name) {
        return fromHttpUrl(getUrl())
                .queryParam("k", this.key)
                .queryParam("i", name)
                .toUriString();
    }
}
