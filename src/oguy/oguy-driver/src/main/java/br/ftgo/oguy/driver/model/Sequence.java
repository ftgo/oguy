package br.ftgo.oguy.driver.model;

import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

public class Sequence {
    private final AtomicLong counter = new AtomicLong(0);

    public long get() {
        return this.counter.get();
    }

    public long increment() {
        while (true) {
            long value0 = get();
            long value1 = value0 + 1;
            if (counter.compareAndSet(value0, value1)) {
                return value1;
            }
        }
    }
}