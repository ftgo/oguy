package br.ftgo.oguy.driver.model;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ultralight {
    private static final Sequence SEQUENCE = new Sequence();

    private static final Pattern MESSAGE_PATTERN = Pattern.compile("((.*)@)?(.*)");

    private static short LEFT_GROUP = 2;

    private static short RIGHT_GROUP = 3;

    private Long id;
    private String name;
    private String payload;

    private Ultralight(Long id, String name, String payload) {
        this.id = id;
        this.name = name;
        this.payload = payload;
    }


    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getPayload() {
        return this.payload;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ultralight message = (Ultralight) o;
        return id == message.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    public static Ultralight fromAgent(String message) {
        Matcher messageRegex = MESSAGE_PATTERN.matcher(message);

        if (!messageRegex.matches())
            throw new IllegalArgumentException(String.format("Malformed agent message (%s)", message));

        String name = messageRegex.group(LEFT_GROUP);
        String payload = messageRegex.group(RIGHT_GROUP);

        return new Ultralight(SEQUENCE.increment(), name, payload);
    }

    public static Ultralight fromDevice(String message) {
        Matcher messageRegex = MESSAGE_PATTERN.matcher(message);

        if (!messageRegex.matches())
            throw new IllegalArgumentException(String.format("Malformed device message (%s)", message));

        Long id = Long.parseLong(messageRegex.group(LEFT_GROUP));

        String payload = messageRegex.group(RIGHT_GROUP);

        return new Ultralight(id, null, payload);
    }

    public String toDevice() {
        return String.format("%d@%s", this.id, this.payload);
    }

    public String toAgent() {
        if (this.name == null) {
            return this.payload;
        }

        return String.format("%s@%s", this.name, this.payload);
    }

    @Override
    public String toString() {
        return "Ultralight{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", payload='" + payload + '\'' +
                '}';
    }
}
