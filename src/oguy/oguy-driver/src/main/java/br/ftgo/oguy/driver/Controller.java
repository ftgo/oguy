package br.ftgo.oguy.driver;

import br.ftgo.oguy.driver.infra.Clock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("oguy/v1")
public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    @Autowired
    private Service service;


    //    @ResponseStatus(OK)
//    MediaType.TEXT_PLAIN
    @PostMapping(value = "/send", produces = "text/plain")
    public String post(@RequestBody final String message) {
        return this.service.send(message);
    }
}
