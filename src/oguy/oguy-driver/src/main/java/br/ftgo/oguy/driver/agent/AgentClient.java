package br.ftgo.oguy.driver.agent;

import br.ftgo.oguy.driver.infra.ErrorHandler;
import br.ftgo.oguy.driver.model.Ultralight;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.MediaType.TEXT_PLAIN;

@Component
public class AgentClient {
    private static final Logger LOGGER = LogManager.getLogger(AgentClient.class);

    private final RestTemplate rest;

    @Autowired
    private AgentProperties properties;

    @Autowired
    public AgentClient(RestTemplateBuilder builder) {
        this.rest = builder.errorHandler(new ErrorHandler()).build();
    }

    private boolean validate(Ultralight ultralight) {
        if (ultralight == null) {
            LOGGER.warn("Ultralight null");
            return false;
        }

        if (ultralight.getPayload() == null) {
            LOGGER.warn("Ultralight payload null");
            return false;
        }

        return true;
    }

    public void send(Ultralight ultralight) {
        if (!validate(ultralight)) return;

        String message = ultralight.getPayload();

        var headers = new HttpHeaders();
        headers.setContentType(TEXT_PLAIN);

        var request = new HttpEntity<String>(message, headers);

        String uri = this.properties.getUri(ultralight.getName());

        this.rest.postForObject(uri, request, Object.class);
    }
}