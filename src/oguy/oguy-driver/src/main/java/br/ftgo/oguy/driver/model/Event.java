package br.ftgo.oguy.driver.model;

import org.springframework.context.ApplicationEvent;

public class Event<T> extends ApplicationEvent {
    private T message;
    private boolean success;

    public Event(Object source, T message, boolean success) {
        super(source);

        this.message = message;
        this.success = success;
    }

    public T getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }
}