package br.ftgo.oguy.driver.serial;

import br.ftgo.oguy.driver.model.Event;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortMessageListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import static com.fazecast.jSerialComm.SerialPort.LISTENING_EVENT_DATA_RECEIVED;

@Component
public class SerialReceiver implements SerialPortMessageListener {
    private static final Logger LOGGER = LogManager.getLogger(SerialReceiver.class);

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public int getListeningEvents() {
        return LISTENING_EVENT_DATA_RECEIVED;

    }

    @Override
    public byte[] getMessageDelimiter() {
        return new byte[]{(byte) 0xD, (byte) 0xA};
    }

    @Override
    public boolean delimiterIndicatesEndOfMessage() {
        return true;
    }

    @Override
    public void serialEvent(SerialPortEvent serialEvent) {
        if (serialEvent.getEventType() != LISTENING_EVENT_DATA_RECEIVED) return;

        byte[] bytes = serialEvent.getReceivedData();

        if (bytes == null) {
            LOGGER.trace("Empty event.");
            return;
        }

        String message = new String(bytes, 0, bytes.length - 2);

        LOGGER.trace("Event: '{}'", message);

        this.publisher.publishEvent(new Event<>(this, message, true));

    }
}