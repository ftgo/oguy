package br.ftgo.oguy.broker.infra;

import br.ftgo.oguy.broker.servlet.LogServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {
    @Bean
    public ServletRegistrationBean logServletBean() {
        ServletRegistrationBean bean = new ServletRegistrationBean(new LogServlet(), "/log/*");
        bean.setLoadOnStartup(1);

        return bean;
    }
}
