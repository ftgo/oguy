/***********************************************************************************************************************
  Specialization    : Doutorado em Ciência da Computação - PPgSC / UFRN
  Course            : Smart Cities Development
  Assignment        : OGUY Firmware
  Reference         : http://learn.makeblock.com/cn/Makeblock-library-for-Arduino/files.html
  Author            : Felipe Toledo G Oliveira <ftgo@ppgsc.ufrn.br>
  Exertion (sRPE)   : Easy
  Analysis          : N/A
  Revision History  :
  Date        Author      Revision (Date in YYYYMMDD format)
  20201113    ftgo        initial creation
  20201125    ftgo        fully functional
  20201126    ftgo        added sensoring controls
  20201127    ftgo        enhanced ultralight protocol; fixed follow logic
  20201218    ftgo        removed results sending of every identified commands; fixed walk duration sent in loop;
                          added walk default duration.  
 **********************************************************************************************************************/

#include <string.h>
#include <stdlib.h>

using namespace std;

#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMCore.h>

// ==============================================================================================================================
// GLOBAL

#define MAIN

#define BAUD_RATE 115200

#define CHARS_SIZE 128
#define CMDS_SIZE 4

#define OK_CODE "OK"
#define FAILED_CODE "FAILED"

#define ID_CMD "id"
#define BLINK_CMD "blink"
#define BUZZ_CMD "buzz"
#define FRONT_CMD "front"
#define BACK_CMD "back"
#define LEFT_CMD "left"
#define RIGHT_CMD "right"
#define FOLLOW_CMD "follow"
#define LIGHT_CMD "light"
#define LINE_CMD "line"
#define ULTRA_CMD "ultra"

#define MOVE_POWER 100
#define MOVE_DURATION 500
#define BLINK_DURATION 1000
#define BUZZ_TIMES 3

#define TIMEOUT 2000

enum direction_t { FRONT, LEFT, RIGHT, BACK };

enum code_t { OK, FAILED };

struct cmd_t {
  char *key = NULL;
  int val = 0;
};

cmd_t cmds[CMDS_SIZE];

char chars[CHARS_SIZE];

bool stopping = false;
bool following = false;

int light_id = -1;
int line_id = -1;
int ultra_id = -1;

bool light_read = false;
bool line_read = false;
bool ultra_read = false;

long light_time0 = 0;
long line_time0 = 0;
long ultra_time0 = 0;

long light_period = TIMEOUT;
long line_period = TIMEOUT;
long ultra_period = TIMEOUT;

// ==============================================================================================================================
// SENSORS

MeUltrasonicSensor ultrasonic_3(3);
MeLineFollower linefollower_2(2);
MeLightSensor lightsensor_6(6);

// ==============================================================================================================================
// MOVE

MeDCMotor motor_9(9);
MeDCMotor motor_10(10);

void move(direction_t direction, int power) {

  int speed = power / 100.0 * 255;

  int leftSpeed = 0;
  int rightSpeed = 0;

  switch (direction) {
    case FRONT:
      leftSpeed = speed;
      rightSpeed = speed;
      break;
    case LEFT:
      leftSpeed = -speed;
      rightSpeed = speed;
      break;
    case RIGHT:
      leftSpeed = speed;
      rightSpeed = -speed;
      break;
    case BACK:
      leftSpeed = -speed;
      rightSpeed = -speed;
      break;
    default:
      break;
  };

  motor_9.run((9) == M1 ? -(leftSpeed) : (leftSpeed));
  motor_10.run((10) == M1 ? -(rightSpeed) : (rightSpeed));
}

void stop() {
  move(FRONT, 0);
}

void walk(direction_t direction, int power, int duration = MOVE_DURATION) {
  power = (power == 0 ? MOVE_POWER : power);
  duration = (duration == 0 ? MOVE_DURATION : duration);

  move(direction, power);

  delay(duration);

  stop();
}

// ==============================================================================================================================
// FOLLOW

void follow(direction_t direction) {
  switch (direction) {
    case FRONT:
      move(FRONT, 45);
      break;
    case LEFT:
      motor_9.run(-1 * 0 / 100.0 * 255);
      motor_10.run(45 / 100.0 * 255);
      break;
    case RIGHT:
      motor_9.run(-1 * 45 / 100.0 * 255);
      motor_10.run(0 / 100.0 * 255);
      break;
    case BACK:
      move(BACK, 45);
      break;
    default:
      break;
  };
}

// ==============================================================================================================================
// BUZZ

MeBuzzer buzzer;

void buzz(int times = BUZZ_TIMES) {
  for (int i = 0; i < times; ++i) {
    buzzer.tone(262, 0.25 * 1000);
  }
}

// ==============================================================================================================================
// LED

// Onboard LED port/slot definitions
const int PORT = 7;
const int SLOT = 2;

// LED Control settings
const int BOTH_LED = 0;
const int RIGHT_LED = 1;
const int LEFT_LED  = 2;

// Declare the MeRGLed object
MeRGBLed led(PORT, SLOT);

void update_led(int id, int red, int green, int blue) {
  led.setColor(id, red, green, blue);
  led.show();
}

void blink_red(int duration = BLINK_DURATION) {
  // set the led color components to generate a shade of red
  int red = 150;
  int green = 0;
  int blue = 0;

  // set the color of the leds to a shade of red
  update_led(BOTH_LED, red, green, blue);

  // wait for wait duration
  delay(duration);

  // set the led color components to generate black (off)
  red = green = blue = 0;

  // set the color of the leds to black (off)
  update_led(BOTH_LED, red, green, blue);
}


// ==============================================================================================================================
// TIMEOUTS

bool light_timeout() {
  long light_time1 = millis();
  if ((light_time1 - light_time0) > light_period) {
    light_time0 = light_time1;
    return true;
  }

  return false;
}

bool line_timeout() {
  long line_time1 = millis();
  if ((line_time1 - line_time0) > line_period) {
    line_time0 = line_time1;
    return true;
  }

  return false;
}


bool ultra_timeout() {
  long ultra_time1 = millis();
  if ((ultra_time1 - ultra_time0) > ultra_period) {
    ultra_time0 = ultra_time1;
    return true;
  }

  return false;
}

// ==============================================================================================================================
// PARSERS

int read_chars() {
  if (!Serial.available())
    return 0;

  memset(chars, 0, CHARS_SIZE);

  int i = 0;
  char ch = -1;
  for (; i < CHARS_SIZE && (ch = Serial.read()) != -1; ++i) {
    chars[i] = ch;

    delay(1); // XXX without this it reads too fast
  }

  // handle ENTER (0xA)
  if (i > 0 && chars[i - 1] == '\n') {
    chars[i - 1] = '\0';
  }

  return i;
}

int index_of(char *chars, char ch) {
  char *at = strchr(chars, ch);
  if (at == NULL)
    return -1;

  return (int) (at - chars);
}



int get_id(char *chars, int length) {
  int pos = index_of(chars, '@');

  if (pos == -1) return -1;

  char id_chars[pos + 1];
  memcpy(id_chars, chars, pos);
  id_chars[pos] = '\0';

  int id = atoi(id_chars);

  return id;
}

int parse_cmds(char *chars) {
  memset(cmds, 0, CMDS_SIZE);

  bool key_turn = true;

  int i = 0;

  int pos = index_of(chars, '@');
  if (pos != -1)
    chars += pos + 1;

  char *token = strtok(chars, "|");

  while (i < CMDS_SIZE && token != NULL) {
    if (key_turn) {
      cmds[i].key = token;
      ++i;
    } else {
      cmds[i - 1].val = atoi(token);
    }

    token = strtok(NULL, "|");

    key_turn = !key_turn;
  }

  return i;
}

// ==============================================================================================================================
// MAIN

void setup() {
  Serial.begin(BAUD_RATE);
}

void loop() {
  int id = -1;
  String results = "";
  bool more = false;

  int light = -1;
  double line = -1;
  double ultra = -1;

  int chars_length = read_chars();
  if (chars_length > 0) {
    id = get_id(chars, chars_length);

    int cmds_length = parse_cmds(chars);

    for (int i = 0; i < cmds_length; ++i) {
      code_t code = OK;

      char *cmd = cmds[i].key;
      int val = cmds[i].val;
      if (strcmp(cmd, ID_CMD) == 0) {
        id = val;
      } else if (strcmp(cmd, BLINK_CMD) == 0) {
        blink_red(500);
      } else if (strcmp(cmd, BUZZ_CMD) == 0) {
        buzz();
      } else if (strcmp(cmd, FRONT_CMD) == 0) {
        walk(FRONT, MOVE_POWER, val);
        if (following) stopping = true;
      } else if (strcmp(cmd, BACK_CMD) == 0) {
        walk(BACK, MOVE_POWER, val);
        if (following) stopping = true;
      } else if (strcmp(cmd, LEFT_CMD) == 0) {
        walk(LEFT, MOVE_POWER, val);
        if (following) stopping = true;
      } else if (strcmp(cmd, RIGHT_CMD) == 0) {
        walk(RIGHT, MOVE_POWER, val);
        if (following) stopping = true;
      } else if (strcmp(cmd, FOLLOW_CMD) == 0) {
        following = !following;
        if (!following) stopping = true;
      } else if (strcmp(cmd, LIGHT_CMD) == 0) {
        light_id = id;
        light_read = !light_read;
        light_period = val == 0 ? TIMEOUT : val;
      } else if (strcmp(cmd, LINE_CMD) == 0) {
        line_id = id;
        line_read = !line_read;
        line_period = val == 0 ? TIMEOUT : val;
      } else if (strcmp(cmd, ULTRA_CMD) == 0) {
        ultra_id = id;
        ultra_read = !ultra_read;
        ultra_period = val == 0 ? TIMEOUT : val;
      } else {
        code = FAILED;
      }
      if (id != -1) {
        if (more) results += "|";

        results += String(cmd) + "|";

        switch (code) {
          case OK:
            results += OK_CODE;
            break;
          case FAILED:
            results += FAILED_CODE;
            break;
          default:
            break;
        }

        more = true;
      }
    }
  }

  if (stopping) {
    stop();
    stopping = false;
  } else if (following) {
    line = linefollower_2.readSensors();
    follow(static_cast<direction_t>(line));
  }

  if (light_read && light_timeout()) {
    if (light == -1) light = lightsensor_6.read();
    Serial.println(String(light_id) + "@" + "light_sensor|" + String(light));
  }

  if (line_read && line_timeout()) {
    if (line == -1) line = linefollower_2.readSensors();
    Serial.println(String(line_id) + "@" + "line_sensor|" + String(line, 2));
  }

  if (ultra_read && ultra_timeout()) {
    if (ultra == -1) ultra = ultrasonic_3.distanceCm();
    Serial.println(String(ultra_id) + "@" + "ultra_sensor|" + String(ultra, 2));
  }

//  if (id != -1) {
//    Serial.println(String(id) + "@" + results);
//  }
}
